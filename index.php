<?
	session_start();
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  	<head>
    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" media="screen">
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="js/bootstrap.js"></script>
		<style>
			html,
			body {
			  height: 100%;
			}

			body {
			  display: -ms-flexbox;
			  display: flex;
			  -ms-flex-align: center;
			  align-items: center;
			  padding-top: 40px;
			  padding-bottom: 40px;
			  background-color: #f5f5f5;
			}

			.form {
			  width: 100%;
			  max-width: 330px;
			  padding: 15px;
			  margin: auto;
			}
			.form-signin .checkbox {
			  font-weight: 400;
			}
			.form-signin .form-control {
			  position: relative;
			  box-sizing: border-box;
			  height: auto;
			  padding: 10px;
			  font-size: 16px;
			}
			.form-signin .form-control:focus {
			  z-index: 2;
			}
			.form-signin input[type="password"] {
			  margin-bottom: 10px;
			  border-top-left-radius: 0;
			  border-top-right-radius: 0;
			}
		</style>
    </head>
	<?php
		$link = new mysqli("localhost", "lebedenkor_game", "8vnSsgj2", "lebedenkor_game");
		$priz = ["money", "fiz", "bonus"]; //типы призов
		$priz_ru = ["money"=>"Деньги", "fiz"=>"Подарок", "bonus"=>"Бонусы"]; //рассшифрока типов
		$fiz = ["Камень", "Сахар", "Сера"]; // виды подарков
		$limit_fiz = 5; //лимит подарков
		$limit_money = 10000; //лимит денег
		$bonus_convert = 10; //коэффициент конвертации денег в рубли
		$err='';
		if ($link->connect_error) {
			die("Connection failed: " . $link->connect_error);
		} 
		if (isset($_POST['login']) && isset($_POST['password'])){
			$login = $_POST['login'];
			$pass = $_POST['password'];
			$select = $link->query("SELECT * FROM user WHERE login='$login' AND pass=md5('$pass')")->fetch_array();
			if ($select){
				$_SESSION['login'] = $select["login"]; //сохраняем логин авторизации
			}
		}
		if (isset($_POST['logout'])){
			unset($_SESSION['login']); // при выходе - убираем логин
		}
		if (isset($_POST['get_priz'])){
			$priz_rand = rand(0,2); // выбираем, какой тип подарка выпал нам
			
			//расчет денег
			if ($priz[$priz_rand]=="money"){
				$priz_get = rand(100, 2000);//вычисление сколько денег получил
				$bonus = $link->query("SELECT balans FROM user_balans WHERE login='{$_SESSION['login']}' AND type='money'");
				$bonus = mysqli_fetch_array($bonus);
				if ($bonus){
					//если есть деньги, обновляем сумму
					if ($bonus["balans"]<$limit_money){
						$summa = $bonus["balans"]+$priz_get;
						$send = $link->query("UPDATE `user_balans` SET `balans`='{$summa}' WHERE login='{$_SESSION['login']}' AND type='money'");
					}else{
						$err = "Достиг лимит денег";
					}					
				}else{
					//добавляем деньги
					$send = $link->query("INSERT INTO `user_balans` (`id`, `login`, `balans`, `type`) VALUES (NULL, '{$_SESSION['login']}', '{$priz_get}', '{$priz[$priz_rand]}')");
				}
			}
			
			//расчет подарков
			if ($priz[$priz_rand]=="fiz"){
				$priz_get = $fiz[rand(0, (count($fiz)-1))]; //вычисление случайного подарка				
				$bonus = $link->query("SELECT * FROM user_balans WHERE login='{$_SESSION['login']}' AND type='fiz'");
				$bonus = mysqli_num_rows($bonus);
				if ($bonus>=$limit_fiz){//проверяем лимит подарков
					$err = "Достиг лимит подарков";
				}else{
					$send = $link->query("INSERT INTO `user_balans` (`id`, `login`, `balans`, `type`) VALUES (NULL, '{$_SESSION['login']}', '{$priz_get}', '{$priz[$priz_rand]}')");
				}
			}
			
			//расчет бонусов
			if ($priz[$priz_rand]=="bonus"){
				$priz_get = rand(100, 2000); //вычисление случайных бонусов
				$bonus = $link->query("SELECT balans FROM user_balans WHERE login='{$_SESSION['login']}' AND type='bonus'");
				$bonus = mysqli_fetch_array($bonus);
				if ($bonus){
					//если есть бонусы - обновить
					$summa = $bonus["balans"]+$priz_get;
					$send = $link->query("UPDATE `user_balans` SET `balans`='{$summa}' WHERE login='{$_SESSION['login']}' AND type='bonus'");
				}else{
					$send = $link->query("INSERT INTO `user_balans` (`id`, `login`, `balans`, `type`) VALUES (NULL, '{$_SESSION['login']}', '{$priz_get}', '{$priz[$priz_rand]}')");
				}
			}
			unset($_GET);
		}
		//конвертация бонусов
		if (isset($_GET['money2bonus'])){
			$money = $link->query("SELECT balans FROM user_balans WHERE login='{$_SESSION['login']}' AND type='money'");
			$money = mysqli_fetch_array($money);
			$bonus = $link->query("SELECT balans FROM user_balans WHERE login='{$_SESSION['login']}' AND type='bonus'");
			$bonus = mysqli_fetch_array($bonus);
			$send_bonus = $money["balans"]*$bonus_convert+$bonus["balans"];
			$send = $link->query("UPDATE `user_balans` SET `balans`='0' WHERE login='{$_SESSION['login']}' AND type='money'");
			$send = $link->query("UPDATE `user_balans` SET `balans`='{$send_bonus}' WHERE login='{$_SESSION['login']}' AND type='bonus'");
			$err = "Вы получили бонусов: ".$money["balans"]*$bonus_convert;
		}
		//выслать подарок
		if (isset($_GET['fiz2mail'])){
			$send = $link->query("DELETE FROM `user_balans` WHERE login='{$_SESSION['login']}' AND id=".intval($_GET['id'])."");
			$err = "Подарок выслали";
		}
		//выслать бонусы
		if (isset($_GET['bonus2prog'])){
			$send = $link->query("UPDATE `user_balans` SET `balans`='0' WHERE login='{$_SESSION['login']}' AND type='bonus'");
			$err = "Бонусы ушли";
		}
		//выслать деньги
		if (isset($_GET['money2bank'])){
			$send = $link->query("UPDATE `user_balans` SET `balans`='0' WHERE login='{$_SESSION['login']}' AND type='money'");
			$err = "Деньги выслали";
		}
		//отказ от подарка
		if (isset($_GET['fiz2null'])){
			$send = $link->query("DELETE FROM `user_balans` WHERE login='{$_SESSION['login']}' AND id=".intval($_GET['id'])."");
			$err = "Подарок аннулирован";
		}
	?>
	<body>
		<? if (!isset($_SESSION['login'])){?>
			<form class="form" method="POST">
			  <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
			  <label for="login" class="sr-only">User name</label>
			  <input type="text" id="login" class="form-control" name="login" placeholder="User name" required autofocus>
			  <label for="password" class="sr-only">Password</label>
			  <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
			  <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
			</form>
		<?}else{?>
			<div class="form">
				<form class="form" method="POST">
					<input type=hidden name=get_priz>
					<button class="btn btn-lg btn-primary btn-block" type="submit">Получить приз</button>
					<? if ($err!=""){
						echo $err;
					}
					?>
					<?
						if (isset($_POST['get_priz']) && $err==""){
							echo "<h5>Вы получили ".$priz_ru[$priz[$priz_rand]]." ".$priz_get."</h5>";
						}
					?>
					<?
						$bonus = $link->query("SELECT * FROM user_balans WHERE login='{$_SESSION['login']}'");
						if ($bonus){
							echo '<h5>Ваш баланс</h5>
							<ul class="list-group mb-3">';
						}
						while($query = mysqli_fetch_array($bonus)){
					?>
							<li class="list-group-item d-flex justify-content-between lh-condensed">
							  <div>
								<h6 class="my-0"><?=$priz_ru[$query["type"]];?></h6>
								<? 
								if($query['type']=="money"){
									echo '<small class="text-muted"><a href="?money2bonus">обменять на бонусы</a></small><BR>';
									echo '<small class="text-muted"><a href="?money2bank">вывести на счет</a></small>';
								}
								?>
								<? 
								if($query['type']=="fiz"){
									echo '<small class="text-muted"><a href="?fiz2mail&id='.$query['id'].'">выслать по почте</a></small><BR>';
									echo '<small class="text-muted"><a href="?fiz2null&id='.$query['id'].'">отказаться от подарка</a></small>';
								}
								?>
								<? 
								if($query['type']=="bonus"){
									echo '<small class="text-muted"><a href="?bonus2prog">перевести в программу</a></small>';
								}
								?>
							  </div>
							  <span class="text-muted"><?=$query["balans"];?></span>
							</li>
						
					<?	}
						if ($bonus){
							echo '</ul>';
						}
					?>
				</form>
				<form class="form" method="POST">
					<input type=hidden name=logout>
					<button class="btn btn-lg btn-primary btn-block" type="submit">Выход</button>
				</form>
			</div>
		<?}?>
	</body>
</html>